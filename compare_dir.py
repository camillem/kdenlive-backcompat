import os
import sys

from compare import compare_videos

def main(video_dir, reference_dir=os.path.join("results", "reference")):
    version = "unknown"
    # assume the last part of the path is the name of the kdenlive build used
    parts = os.path.split(video_dir)
    if parts and parts[1] == "":
        parts = os.path.split(parts[0])
    if parts:
        version = parts[1]
        print("Assuming that the kdenlive build is \"%s\"" % (version,))
    missing_references = []
    for video in os.listdir(video_dir):
        # check that there's a corresponding reference file
        if not os.path.exists(os.path.join(reference_dir, video)):
            missing_references.append(video)

    if missing_references:
        print("The following videos are missing reference files:")
        for video in missing_references:
            print("  - %s" % video)
        print("Continue? [y/N]")
        choice = input().lower()
        if not choice or choice != "y":
            return 1

    results = []
    for video in os.listdir(video_dir):
        if os.path.join(reference_dir, video) in missing_references:
            continue
        print("Comparing %s against reference" % video)
        if video.endswith(".mp4"):
            vid_result = compare_videos(os.path.join(reference_dir, video), os.path.join(video_dir, video))
            print_results(video, version, vid_result)


def print_results(video, version, res):
    print("""  - project: {0}
        version: {1}
        expected_frames: {2}
        actual_frames: {3}
        different_frames: {4}
        bugs: []""".format(
            video, version,
            res.video1_frames,
            res.video2_frames,
            res.different_frames
        ))

if __name__ == "__main__":
    sys.exit(main(sys.argv[1]))
