import yaml

def main():
    with open('test-cases.yaml', 'r') as f:
        test_cases = yaml.safe_load(f)
    with open('test-results.yaml', 'r') as f:
        test_results = yaml.safe_load(f)

    print("""<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Kdenlive test results</title>
    <style>
        .result.good {
            color: green;
        }
        .result.bad {
            color: red;
        }
        table {
            border-collapse: collapse;
        }
        td, th {
            border: 2px solid black;
        }
        td.result {
            text-align: center;
        }
    </style>
</head>
<body>
<table>
<tr>
    <th>Test case</th>
""")
    for ver in test_results['tested_versions']:
        print("    <th>{}</th>".format(ver))
    print("</tr>")

    for details in test_cases:
        print("<tr>")
        print("    <td>{}</td>".format(details['name']))
        for ver in test_results['tested_versions']:
            for result in test_results['tested_projects']:
                if result['project'] == details['name'] and result['version'] == ver:
                    percentage_diff = (1-result['different_frames']/result['actual_frames'])*100
                    if percentage_diff == 100 and result['expected_frames'] == result['actual_frames']:
                        result_class = "good"
                    else:
                        result_class = "bad"
                    print("    <td class=\"result {}\">{:.1f}%".format(result_class, percentage_diff))
                    if result['expected_frames'] != result['actual_frames']:
                        print("<br />⚠️ Expected {} frames, got {}".format(result['expected_frames'], result['actual_frames']))
                    if 'bugs' in result and result['bugs']:
                        print("<br />🪲 Bugs: {}".format(", ".join([
                            "<a href=\"https://invent.kde.org/multimedia/kdenlive/-/issues/{0}\">#{0}</a>".format(bid) for bid in result['bugs']
                        ])))
                    break
            else:
                print("<td class=\"result no-result\">?")
            print("</td>")
        print("</tr>")

    print("""
</table>
</body>
</html>
    """)

if __name__ == "__main__":
    main()
