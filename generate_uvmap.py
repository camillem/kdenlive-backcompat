import cv2
import numpy

"""
This script generates an idempotent UV map for the frei0r.uvmap effect. Using
this UV map should theoretically result in the same output as the input,
although due to rounding the output may look pixelated.
"""

# create 1920x1080 16-bit image
img = numpy.zeros((1080, 1920, 3), numpy.uint16)
MAX_INT = 2**16 - 1

for y in range(1080):
    for x in range(1920):
        img[y][x] = [MAX_INT, -1 - y * MAX_INT / 1080, x * MAX_INT / 1920]

# save RGB img as output.png
cv2.imwrite("output.png", img)
