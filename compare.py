import argparse
import sys

from skimage.metrics import structural_similarity
from tqdm import tqdm
import cv2
import numpy

#
# This program takes two video files and compares them frame by frame. It
# calculates how many frames differed beyond the MSE_THRESHOLD, and which frame
# was the first different frame.
#
# Usage:
#  python compare.py video1.mp4 video2.mp4
#

MSE_THRESHOLD = 250 # using 8-bit pixels
SSIM_THRESHOLD = 0.95 # scale of -1 to 1
BLOB_AREA_THRESHOLD = 20 # blob area in pixels
BLOB_GRAYSCALE_THRESHOLD = 80 # 80 is < 255/3, i.e. one channel being totally different


class CompareResults():
    video1_frames = 0
    video2_frames = 0
    different_frames = 0

    def __init__(self, video1_frames, video2_frames, different_frames):
        self.video1_frames = video1_frames
        self.video2_frames = video2_frames
        self.different_frames = different_frames


def compare_videos(vid1, vid2, stop_when_different=False, display_difference=False, save_difference=False, verbose=False):
    cap1 = cv2.VideoCapture(vid1)
    cap2 = cv2.VideoCapture(vid2)

    if cap1.get(cv2.CAP_PROP_FRAME_WIDTH) != cap2.get(cv2.CAP_PROP_FRAME_WIDTH):
        print("Video width is not equal")
        sys.exit(1)
    if cap1.get(cv2.CAP_PROP_FRAME_HEIGHT) != cap2.get(cv2.CAP_PROP_FRAME_HEIGHT):
        print("Video height is not equal")
        sys.exit(1)

    if cap1.get(cv2.CAP_PROP_FPS) != cap2.get(cv2.CAP_PROP_FPS):
        print("Video fps is not equal: {} != {}".format(cap1.get(cv2.CAP_PROP_FPS), cap2.get(cv2.CAP_PROP_FPS)))

    cap1frames = int(cap1.get(cv2.CAP_PROP_FRAME_COUNT))
    cap2frames = int(cap2.get(cv2.CAP_PROP_FRAME_COUNT))
    if cap1frames != cap2frames:
        print("Video frame count is not equal: {} != {}".format(cap1.get(cv2.CAP_PROP_FRAME_COUNT), cap2.get(cv2.CAP_PROP_FRAME_COUNT)))

    first_different_frame = None
    first_different_frame1 = None
    first_different_frame2 = None
    first_different_frames_sidebyside = None
    diff_frames = 0
    max_mse = 0
    min_ssim = 1
    max_blobs = 0
    blob_detector_params = cv2.SimpleBlobDetector_Params()
    blob_detector_params.filterByArea = True
    blob_detector_params.minArea = BLOB_AREA_THRESHOLD
    blob_detector_params.filterByColor = True
    blob_detector_params.blobColor = 255
    blob_detector_params.filterByCircularity = False
    blob_detector_params.filterByConvexity = False
    # in opencv < 3, the constructor is SimpleBlobDetector(blob_detector_params)
    blob_detector = cv2.SimpleBlobDetector_create(blob_detector_params)

    for i in tqdm(range(min(cap1frames, cap2frames))):
        _, frame1 = cap1.read()
        _, frame2 = cap2.read()

        if frame1 is None or frame2 is None:
            print("Ran out of frames")
            break

        diff_frame = numpy.zeros(frame1.shape, numpy.int16)
        diff_frame += frame1
        diff_frame -= frame2
        mse = numpy.square(diff_frame).mean()
        # disabling ssim for now, doesn't seem to be useful
        ssim = 1 #structural_similarity(frame1, frame2, multichannel=True)

        diff_frame = numpy.abs(diff_frame)
        grayscale_diff_frame = cv2.cvtColor(diff_frame.astype('uint8'), cv2.COLOR_BGR2GRAY) * 3
        grayscale_diff_frame[grayscale_diff_frame > BLOB_GRAYSCALE_THRESHOLD] = 255
        grayscale_diff_frame[grayscale_diff_frame <= BLOB_GRAYSCALE_THRESHOLD] = 0
        blobs = blob_detector.detect(grayscale_diff_frame)
        # for debugging grayscale_diff_frame
        #if blobs:
        #    cv2.imshow("grayscale_diff_frame", grayscale_diff_frame)
        #    cv2.waitKey()

        #print("Frame {}: {}".format(i, diff))

        if mse > MSE_THRESHOLD or ssim < SSIM_THRESHOLD or len(blobs) > 0:
            if verbose:
                if mse > MSE_THRESHOLD:
                    print("Frame {} has mse {}".format(i, mse))
                elif ssim < SSIM_THRESHOLD:
                    print("Frame {} has ssim {}".format(i, ssim))
                elif len(blobs) > 0:
                    print("Frame {} has {} blobs of difference".format(i, len(blobs)))
                    print(list([x.pt for x in blobs]))
                    #cv2.imshow("grayscale_diff_frame", grayscale_diff_frame)
                    #cv2.waitKey()
            if first_different_frame is None:
                first_different_frame1 = frame1
                first_different_frame2 = frame2
                first_different_frame = i
                first_different_frames_sidebyside = numpy.hstack((frame1, frame2))
            diff_frames += 1
            if stop_when_different:
                break
        if mse > max_mse:
            max_mse = mse
        #if ssim < min_ssim:
        #    min_ssim = ssim
        if len(blobs) > max_blobs:
            max_blobs = len(blobs)

    print("First different frame: {}".format(first_different_frame))
    print("Maximum average difference: {}".format(max_mse))
    #print("Minimum SSim: {}".format(min_ssim))
    print("Maximum number of blobs: {}".format(max_blobs))
    print("Different frames: {}/{} ({:.2f}%)"
        .format(diff_frames,
            min(cap1frames, cap2frames),
            diff_frames / min(cap1frames, cap2frames) * 100))

    if first_different_frame is not None:
        if display_difference:
            cv2.imshow("Frame {}".format(first_different_frame), first_different_frames_sidebyside)
            cv2.waitKey()
        if save_difference:
            cv2.imwrite("difference1.png", first_different_frame1)
            cv2.imwrite("difference2.png", first_different_frame2)
            print("Frames saved as difference1.png and difference2.png")
    return CompareResults(cap1frames, cap2frames, diff_frames)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("vid1", help="First video file")
    parser.add_argument("vid2", help="Second video file")
    parser.add_argument("--stop-when-different", action="store_true", help="Stop when first difference is found")
    parser.add_argument("--display-difference", action="store_true", help="Display the first different frame")
    parser.add_argument("--save-difference", action="store_true", help="Save the first different frames as difference1.png and difference2.png")
    parser.add_argument("--verbose", action="store_true", help="Print more information about what frames are different")
    args = parser.parse_args()

    results = compare_videos(args.vid1, args.vid2, args.stop_when_different, args.display_difference, args.save_difference, args.verbose)
    if results.video1_frames == results.video2_frames and results.different_frames == 0:
        return 0
    else:
        return 1

if __name__ == "__main__":
    sys.exit(main())
